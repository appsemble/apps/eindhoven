import { expect } from '@playwright/test';

import { formatDate, getTableRowByDate, setTestDate } from './helpers.js';
import { test } from '../../fixtures/test/index.js';

let testReservationDate: Date;

test.describe('Werkplek Reservering', () => {
  test.skip();

  test.beforeEach(async ({ loginApp, studioLogin, visitApp }) => {
    await studioLogin();
    await visitApp('werkplek-reservering');
    await loginApp();
    testReservationDate = setTestDate();
  });

  test('should create a reservation', async ({ page }) => {
    await page.click('a[href*="/reservations"]');

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    // Creating a reservation with the same properties can cause an error
    // If it already exists, we skip this step
    if (page.url().includes('/reservations')) {
      return;
    }

    const formattedDate = formatDate(testReservationDate, false);

    await page.click('a[href*="/make-reservation"]');

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await page.click('input[id="date"]');
    await page.click(`span[aria-label*="${formattedDate}"]`);

    await page.waitForSelector('.numInputWrapper > input', { state: 'visible' });

    const hoursString = String(testReservationDate.getHours());
    await page.fill('.numInputWrapper > input[class="numInput flatpickr-hour"]', hoursString);

    const minutesString = String(testReservationDate.getMinutes());
    await page.fill('.numInputWrapper > input[class="numInput flatpickr-minute"]', minutesString);

    await page.click('.flatpickr-confirm');
    await page.waitForSelector('.flatpickr-calendar', { state: 'hidden' });

    // If the starting hour is anything other than 10 AM, the end date field is recalculated
    // This means that it is reset to default after it is set by the test
    // And this results in failing to submit the form properly
    // For now this is the only workaround found, if test fails consistently
    await page.waitForTimeout(500);

    await page.selectOption('select[id="endDate"]', { index: 3 });

    await page.waitForSelector('select[id="workplaceType"]', { state: 'visible' });
    await page.selectOption('select[id="workplaceType"]', { index: 1 });

    await page.waitForSelector('select[id="deskType"]', { state: 'visible' });
    await page.selectOption('select[id="deskType"]', { index: 0 });

    await page.waitForSelector('select[id="workplaceCode"]', { state: 'visible' });
    await page.selectOption('select[id="workplaceCode"]', { index: 1 });

    await page.click('button[type="submit"]');

    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await page.click('a[href*="/reservations"]');
  });

  test('should return an error if there is a reservation at the time', async ({ page }) => {
    const formattedDate = formatDate(testReservationDate, false);

    await page.click('a[href*="/make-reservation"]');

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await page.click('input[id="date"]');
    await page.click(`span[aria-label*="${formattedDate}"]`);

    await page.waitForSelector('.numInputWrapper > input', { state: 'visible' });

    const hoursString = String(testReservationDate.getHours());
    await page.fill('.numInputWrapper > input[class="numInput flatpickr-hour"]', hoursString);

    const minutesString = String(testReservationDate.getMinutes());
    await page.fill('.numInputWrapper > input[class="numInput flatpickr-minute"]', minutesString);

    await page.click('.flatpickr-confirm');

    // If the starting hour is anything other than 10 AM, the end date field is recalculated
    // This means that it is reset to default after it is set by the test
    // And this results in failing to submit the form properly
    // For now this is the only workaround found
    await page.waitForTimeout(500);

    await page.selectOption('select[id="endDate"]', { index: 2 });

    await page.waitForSelector('form > .is-danger > .message-body > span');
  });

  test('should view and edit a reservation', async ({ page }) => {
    await page.click('a[href*="/reservations"]');

    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    expect(page.url()).toContain('/reservations');

    const formattedDate = formatDate(testReservationDate);

    const tableRows = await page.$$('tbody tr');
    const row = await getTableRowByDate(formattedDate, tableRows);
    const oldRowDate = await (await row.$$('td'))[1].textContent();

    await page.click('.dropdown-trigger > button');

    // The view and delete button are almost identical with the exception of inner text
    // Inner text changes with locale settings so its not a reliable selector
    // Rely the view button is always first
    const viewOption = (await row.$$('.dropdown-content > button'))[0];
    await viewOption.click();

    const card = page.locator('div[class="modal-card"]');
    await expect(card).toBeVisible();

    const editButton = card.locator('a.button');
    await editButton.click();

    await page.waitForSelector('select[id="endDate"]');

    // If the starting hour is anything other than 10 AM, the end date field is recalculated
    // This means that it is reset to default after it is set by the test
    // And this results in failing to submit the form properly
    // For now this is the only workaround found
    await page.waitForTimeout(500);
    await page.selectOption('select[id="endDate"]', { index: 1 });

    await page.click('button[type="submit"]');

    await page.click('a[href*="/reservations"]');

    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    expect(page.url()).toContain('/reservations');

    // Check if the new end date is different than the old one
    await page.waitForSelector('table');
    const updatedTableRows = await page.$$('tbody tr');
    const updatedRow = await getTableRowByDate(formattedDate, updatedTableRows);
    const cell = (await updatedRow.$$('td'))[1];
    const updatedDate = await cell.textContent();

    expect(updatedDate).toContain(formattedDate);
    expect(updatedDate !== oldRowDate).toBeTruthy();
  });

  test('should delete a reservation', async ({ page }) => {
    await page.click('a[href*="/reservations"]');

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    const formattedDate = formatDate(testReservationDate);

    const tableRows = await page.$$('tbody tr');
    const count = tableRows.length;

    const row = await getTableRowByDate(formattedDate, tableRows);

    await page.click('.dropdown-trigger > button');

    // The view and delete button are almost identical with the exception of inner text
    // Inner text changes with locale settings so its not a reliable selector
    // Rely the delete button is always second
    const deleteOption = (await row.$$('.dropdown-content > button'))[1];
    await deleteOption.click();

    await page.waitForSelector('button[class*="is-danger"]');

    await page.click('button.is-danger');
    if (count > 1) {
      await page.reload();
      await page.waitForSelector('table', { state: 'visible' });

      expect((await page.$$('tbody tr')).length).toBe(count - 1);
    } else {
      await page.waitForURL('**/make-reservation');
    }
  });

  // Test works only if ran during working hours, on a weekday
  // By design, reservations can only be created during those times
  // And check-in can happen only between the start and end time of a reservation
  // Test is skipped to avoid pipeline failures if ran at an inappropriate time
  test.skip('should check in and out of a reservation', async ({ page }) => {
    const date = new Date();
    const formattedDate = formatDate(date, false);

    await page.click('a[href*="/make-reservation"]');

    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await page.click('input[id="date"]');
    await page.click(`span[aria-label*="${formattedDate}"]`);

    await page.waitForSelector('.numInputWrapper > input', { state: 'visible' });

    const hoursString = String(date.getHours());
    await page.fill('.numInputWrapper > input[class="numInput flatpickr-hour"]', hoursString);

    const minutesString = String(date.getMinutes() + 5);

    await page.fill('.numInputWrapper > input[class="numInput flatpickr-minute"]', minutesString);

    await page.click('.flatpickr-confirm');
    await page.waitForSelector('.flatpickr-calendar', { state: 'hidden' });

    await page.selectOption('select[id="endDate"]', { index: 3 });

    await page.selectOption('select[id="workplaceType"]', { index: 0 });

    await page.selectOption('select[id="deskType"]', { index: 0 });

    await page.selectOption('select[id="workplaceCode"]', { index: 1 });

    await page.click('button[type="submit"]');

    await page.click('a[href*="/reservations"]');

    await page.waitForSelector('tbody tr');
    // Check in
    await page.waitForSelector('td > div > button');

    await page.click('td > div > button');

    // Checkout
    await page.click('td > div > button');
  });
});
