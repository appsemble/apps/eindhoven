# PDF Generator API

## Run

```bash
npm start
```

## Variables

The server can be configured using the following environment variables:

| Variable | Default                | Description                                                |
| -------- | ---------------------- | ---------------------------------------------------------- |
| `PORT`   | `3000`                 | The port to listen on                                      |
| `SECRET` |                        | The secret key to use for securing the API                 |
| `APP_ID` |                        | The ID of the app that will use this API                   |
| `REMOTE` | `http://appsemble.app` | The Appsemble host where the app using this API is running |

## Deployment

### Secrets

```sh
kubectl create secret generic pdf-generator-api \
  --namespace eindhoven \
  --from-literal="secret=$(uuidgen)"
```

### Installing

```sh
# assuming you are in the same directory as this README - otherwise, use the full path
helm install pdf-generator . \
  --namespace eindhoven \
  --set 'appId=SOME_APP_ID' \
  --set 'remote=REMOTE_URL'
```

Where `SOME_APP_ID` is the ID of the app that will use this API, and `REMOTE_URL` is the URL of the
Appsemble host where that app is running.

The PDF generator is now available from
`https://pdf-generator-pdf-generator-api.eindhoven.svc.cluster.local` within the cluster.
