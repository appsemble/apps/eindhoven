import Router from '@koa/router';

import { health } from '../handlers/health.js';
import { render } from '../handlers/render.js';

const router = new Router();

router.get('/health', health);
router.post('/render', render);

export const routes = router.routes();
export const allowedMethods = router.allowedMethods();
