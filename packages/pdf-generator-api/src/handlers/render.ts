import { writeFile } from 'node:fs/promises';

import { logger } from '@appsemble/node-utils';
import carbone from 'carbone';
import { withFile as withTmpFile } from 'tmp-promise';

import { type Context } from '../../types.js';

export async function render(ctx: Context): Promise<void> {
  const token = ctx.headers.authorization?.replace(/^Bearer /, '');
  ctx.assert(token === ctx.argv.secret, 401);
  ctx.assert(ctx.request.body, 400, 'Request body is required');
  ctx.assert(
    typeof ctx.request.body === 'object' && !Array.isArray(ctx.request.body),
    400,
    'Request body must be an object',
  );
  const { params, template } = ctx.request.body as Record<string, unknown>;
  ctx.assert(template, 400, 'Template asset is required');
  ctx.assert(typeof template === 'string', 400, 'Template asset must be a string');
  ctx.assert(params, 400, 'Template params are required');
  ctx.assert(
    typeof params === 'object' && !Array.isArray(params),
    400,
    'Template params must be an object',
  );

  const templateUrl = `${ctx.argv.remote}/api/apps/${ctx.argv.appId}/assets/${encodeURIComponent(
    template,
  )}`;
  logger.verbose(`Fetching template from ${templateUrl}`);
  const templateFile = await fetch(templateUrl)
    .then((response) => response.blob())
    .then((blob) => blob.arrayBuffer())
    .then((arrayBuffer) => Buffer.from(arrayBuffer));

  await withTmpFile(
    async ({ path: templatePath }) => {
      logger.verbose(`Storing template file in ${templatePath}`);
      await writeFile(templatePath, templateFile);
      const result = await new Promise<Buffer | string>((resolve, reject) => {
        carbone.render(templatePath, params, { convertTo: 'pdf' }, (err, res) =>
          err ? reject(err) : resolve(res),
        );
      });
      ctx.set('Content-Type', 'application/pdf');
      ctx.body = result;
    },
    {
      filename: template,
      postfix: '.odt',
    },
  );
}
