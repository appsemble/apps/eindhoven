import { configureLogger, logger, loggerMiddleware } from '@appsemble/node-utils';
import { bodyParser } from '@koa/bodyparser';
import cors from '@koa/cors';
import Koa from 'koa';

import { allowedMethods, routes } from './routes/index.js';
import { type Argv } from '../types.js';

for (const envVar of ['SECRET', 'APP_ID']) {
  if (!process.env[envVar]) {
    logger.error(`Environment variable ${envVar} is required`);
    process.exit(1);
  }
}

if (Number.isNaN(Number.parseInt(process.env.APP_ID!))) {
  logger.error('Environment variable APP_ID must be a number');
  process.exit(1);
}

const args: Argv = {
  port: process.env.PORT ? Number.parseInt(process.env.PORT) : 3000,
  secret: process.env.SECRET,
  verbose: process.env.VERBOSE ? Number.parseInt(process.env.VERBOSE) : 0,
  quiet: process.env.QUIET ? Number.parseInt(process.env.QUIET) : 0,
  remote: process.env.REMOTE ?? 'https://appsemble.app',
  appId: Number.parseInt(process.env.APP_ID!),
};

configureLogger({ quiet: args.quiet, verbose: args.verbose });
const app = new Koa();
app.context.argv = args;
app.use(cors({ credentials: true }));
app.use(bodyParser());
app.use(loggerMiddleware());
app.on('error', (err) => {
  logger.error(err);
});
app.use(routes);
app.use(allowedMethods);

// https://github.com/carboneio/carbone/issues/181
for (const signal of ['SIGINT', 'SIGHUP', 'SIGQUIT']) {
  process.on(signal, () => {
    logger.info(`Received ${signal}, exiting`);
    process.exit(0);
  });
}

app.listen(args.port, () => {
  logger.info(`Listening on http://localhost:${args.port}`);
});
