import assert from 'node:assert/strict';
import { writeFile } from 'node:fs/promises';
import { createInterface } from 'node:readline/promises';
import { parseArgs } from 'node:util';

import { type Workplace } from '@eindhoven/types';
import xlsx from 'xlsx';

interface TokenResponse {
  access_token: string;
}

const helpMessage = `Usage: npm run sync [OPTIONS]... FILE...

Synchronize an XLSX file containing workspace information with Appsemble.

This script requires the CLIENT_CREDENTIALS environment variable to be set.

Options:

  --id      The app ID to use.
  --remote  The Appsemble remote to use.
  --help    Print this help message.
`;

const {
  positionals: [filePath],
  values: { help, id = 292, remote = 'https://appsemble.app' },
} = parseArgs({
  allowPositionals: true,
  options: {
    id: { type: 'string' },
    remote: { type: 'string' },
    help: { type: 'boolean' },
  },
});

/**
 * Print the help message and exit the process.
 *
 * @param exitCode The process exit code.
 */
function die(exitCode = 1): never {
  console.log(helpMessage);
  process.exit(exitCode);
}

/**
 * Ask the user if they want to continue execution.
 *
 * @param message The prompt message to display.
 */
async function promptContinue(message: string): Promise<void> {
  const rl = createInterface({ input: process.stdin, output: process.stdout });
  const answer = await rl.question(`${message}? y/N\n`);
  rl.close();

  if (answer.toLowerCase() !== 'y') {
    process.exit();
  }
}

/**
 * Safely get a value from an unknown object.
 *
 * @param obj The obj to get a value from.
 * @param key The key to get.
 * @param expected An array of expected values. If specified, the value must be in this array. The
 *   value must be lower case.
 * @returns The retrieved value.
 */
function safeGet<E>(obj: object, key: string, expected?: readonly E[]): E {
  assert(key in obj, `Missing field: ${JSON.stringify(key)}`);
  const value = (obj as Record<string, unknown>)[key];
  if (expected) {
    const lower = (typeof value === 'string' ? value.toLowerCase() : value) as E;
    assert(
      expected.includes(lower),
      `Expected one of: ${JSON.stringify(expected)}, got: ${JSON.stringify(value)}`,
    );
    return lower;
  }
  return value as E;
}

/**
 * Read workplaces from an XLSX sheet.
 *
 * @param path The file path to the XLSX sheet to read.
 * @returns The list of workplaces in the sheet.
 */
function readSheet(path: string): Workplace[] {
  const workbook = xlsx.readFile(path, { sheets: 'werkplekken' });
  assert('werkplekken' in workbook.Sheets);

  const workplaces: Workplace[] = [];
  outer: for (const row of xlsx.utils.sheet_to_json(workbook.Sheets.werkplekken)) {
    assert(typeof row === 'object');
    assert.ok(row);

    const canBeReserved = safeGet(row, 'Te reserveren', ['ja', 'nee'] as const);
    if (canBeReserved === 'nee') {
      // Assuming this should be ignored.
      continue;
    }

    const building = safeGet(row, 'Gebouw (verkort)');
    const floor = safeGet(row, 'Etage / vloerveld');

    const sheetWorkplaceCode = String(safeGet(row, 'ruimtenaam compleet (uniek ID)'));
    const workplaceCodeSegments = sheetWorkplaceCode.split('.');
    workplaceCodeSegments.pop();
    const workplaceCode = workplaceCodeSegments.join('.');

    const capacity = safeGet(row, 'ruimtecapaciteit (max.)');
    assert(typeof capacity === 'number');
    assert(capacity >= 0 && Number.isInteger(capacity));

    const sheetWorkplaceType = safeGet(row, 'Type Werkzone', [
      'routine',
      'focus',
      'samenwerk',
    ] as const);
    const workplaceType =
      sheetWorkplaceType === 'samenwerk'
        ? 'Samenwerken'
        : sheetWorkplaceType === 'focus'
          ? 'Focus'
          : 'Routine';

    const sheetDeskType = safeGet(row, 'Type werkplek', [
      'cockpit/standaard',
      'cockpit/zit-sta',
      'open/standaard',
      'open/zit-sta',
    ] as const);
    const deskType =
      sheetDeskType === 'open/standaard'
        ? 'Open / Standaard'
        : sheetDeskType === 'open/zit-sta'
          ? 'Open / Zit-Sta'
          : sheetDeskType === 'cockpit/standaard'
            ? 'Cockpit / Standaard'
            : 'Cockpit / Zit-Sta';

    for (const existingWorkplace of workplaces) {
      if (
        existingWorkplace.workplaceCode === workplaceCode &&
        existingWorkplace.workplaceType === workplaceType &&
        existingWorkplace.deskType === deskType
      ) {
        existingWorkplace.capacity += capacity;
        continue outer;
      }
    }

    workplaces.push({
      workplaceName: `${building}.${floor}`,
      workplaceCode,
      capacity,
      workplaceType,
      deskType,
      maintenance: [],
    });
  }
  return workplaces.sort(
    (a, b) =>
      a.workplaceCode.localeCompare(b.workplaceCode) ||
      a.workplaceName.localeCompare(b.workplaceName) ||
      a.workplaceType.localeCompare(b.workplaceType) ||
      a.deskType.localeCompare(b.deskType),
  );
}

/**
 * Create a map that maps a workplace code to a workplace.
 *
 * @param workplaces The workplaces to create a map for.
 * @returns A map of workplaces.
 */
function workplacesToMap(workplaces: Workplace[]): Map<string, Workplace> {
  const map = new Map<string, Workplace>();
  for (const workplace of workplaces) {
    map.set(workplace.workplaceCode, workplace);
  }
  return map;
}

let authorization: string;

async function request<T>(url: string, method = 'GET', data?: unknown): Promise<T> {
  const headers: Record<string, string> = {};
  if (authorization) {
    headers.authorization = authorization;
  }
  let body;
  if (data) {
    if (data instanceof URLSearchParams) {
      body = String(data);
      headers['content-type'] = 'application/x-www-form-urlencoded';
    } else {
      body = JSON.stringify(data);
      headers['content-type'] = 'application/json';
    }
  }
  const response = await fetch(new URL(url, remote), { method, body, headers });
  const responseBody: unknown = response.headers.get('content-type')?.includes('application/json')
    ? await response.json()
    : await response.text();
  if (response.ok) {
    return responseBody as T;
  }

  throw new Error(response.statusText, { cause: responseBody });
}

async function login(): Promise<void> {
  const credentials = safeGet(process.env, 'CLIENT_CREDENTIALS');
  assert(typeof credentials === 'string');

  const [clientId, clientSecret] = credentials.split(':');
  const encoded = Buffer.from(`${clientId}:${clientSecret}`).toString('base64');
  authorization = `Basic ${encoded}`;

  const response = await request<TokenResponse>(
    '/oauth2/token',
    'POST',
    new URLSearchParams({
      grant_type: 'client_credentials',
      scope: 'resources:read resources:write',
    }),
  );
  const token = safeGet(response, 'access_token');
  authorization = `Bearer ${token}`;
}

if (help) {
  die(0);
}

if (!filePath) {
  die();
}

const appId = Number(id);
assert(!Number.isNaN(appId));

const sheetWorkplacesArray = readSheet(filePath);

/**
 * The workplaces that are available in the XLSX spreadsheet.
 */
const sheetWorkplaces = workplacesToMap(sheetWorkplacesArray);

await login();

/**
 * The workplaces that are available in the Appsemble API.
 */
const apiWorkplaces = workplacesToMap(await request(`/api/apps/${appId}/resources/workplace`));

/**
 * The resource IDs of workspaces that will be deleted.
 */
const toDelete = new Set<Workplace>();

/**
 * The resource IDs of workspaces that will be deleted.
 */
const toUpdate = new Set<Workplace>();

/**
 * The resource IDs of workspaces that will be deleted.
 */
const toKeep = new Set<Workplace>();

/**
 * The resource IDs of workspaces that will be deleted.
 */
const toCreate = new Set<Workplace>();

for (const workplace of apiWorkplaces.values()) {
  if (!sheetWorkplaces.has(workplace.workplaceCode)) {
    toDelete.add(workplace);
    apiWorkplaces.delete(workplace.workplaceCode);
  }
}

for (const workplace of sheetWorkplaces.values()) {
  const apiWorkspace = apiWorkplaces.get(workplace.workplaceCode);
  if (!apiWorkspace) {
    toCreate.add(workplace);
    continue;
  }

  if (
    apiWorkspace.capacity === workplace.capacity &&
    apiWorkspace.deskType === workplace.deskType &&
    apiWorkspace.workplaceName === workplace.workplaceName &&
    apiWorkspace.workplaceType === workplace.workplaceType
  ) {
    toKeep.add(apiWorkspace);
  } else {
    toUpdate.add({
      ...workplace,
      maintenance: apiWorkspace.maintenance ?? [],
      id: apiWorkspace.id,
    });
  }
}

const localResourcesPath = new URL(
  '../../apps/werkplek-reservering/resources/workplace.json',
  import.meta.url,
);
await promptContinue(`Do you want to update the local resources in ${localResourcesPath.pathname}`);
await writeFile(
  new URL(localResourcesPath, import.meta.url),
  `${JSON.stringify([...sheetWorkplaces.values()], undefined, 2)}\n`,
);
console.log(`Updated ${localResourcesPath.pathname}`);

console.log('Continuing would perform the following actions:');
for (const workplace of toKeep) {
  console.log(`Keep resource ${workplace.id} (workplace ${workplace.workplaceCode})`);
}

for (const workplace of toCreate) {
  console.log(`Create workplace ${workplace.workplaceCode}`);
}

for (const workplace of toUpdate) {
  console.log(`Update resource ${workplace.id} (workplace ${workplace.workplaceCode})`);
}

for (const workplace of toDelete) {
  console.log(`Delete resource ${workplace.id} (workplace ${workplace.workplaceCode})`);
}

await promptContinue('Do you want to continue');

for (const workplace of toCreate) {
  console.log(`Creating workplace ${workplace.workplaceCode}`);
  await request(`/api/apps/${appId}/resources/workplace`, 'POST', workplace);
}

for (const workplace of toUpdate) {
  console.log(`Updating resource ${workplace.id} (workplace ${workplace.workplaceCode})`);
  await request(`/api/apps/${appId}/resources/workplace/${workplace.id}`, 'PUT', workplace);
}

for (const workplace of toDelete) {
  console.log(`Deleting resource ${workplace.id} (workplace ${workplace.workplaceCode})`);
  await request(`/api/apps/${appId}/resources/workplace/${workplace.id}`, 'DELETE');
}
