FROM node:18

# https://github.com/carboneio/carbone-env-docker/blob/master/Dockerfile
# https://github.com/carboneio/carbone-env-docker/pull/3/commits/785ee21cbcae252f15d37588c3af936b3bd49024

COPY . /app
WORKDIR /app

RUN apt-get update \
 && apt-get install -y \
      lsof \
      libxinerama1 \
      libfontconfig1 \
      libdbus-glib-1-2 \
      libcairo2 \
      libcups2 \
      libglu1-mesa \
      libsm6 \
      unzip \
      libnss3 \
      curl \
 && wget -q -O- https://downloadarchive.documentfoundation.org/libreoffice/old/7.5.1.1/deb/x86_64/LibreOffice_7.5.1.1_Linux_x86-64_deb.tar.gz | tar -xzvf - \
 && dpkg -i LibreOffice_*/DEBS/* \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf LibreOffice_* \
 && npm ci \
 && npm --workspaces run --if-present build

EXPOSE 3000

USER node

ENTRYPOINT ["node", "packages/pdf-generator-api/src/index.js"]
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl --fail http://localhost:3000/health || exit 1
