This block contains custom logic for the
[Werkplek Reservering](https://gitlab.com/appsemble/eindhoven/-/tree/main/apps/werkplek-reservering)
app of the Municipality of Eindhoven.
