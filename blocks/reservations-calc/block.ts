export {};

declare module '@appsemble/sdk' {
  interface Messages {
    /**
     * The label for a workplace including its availability
     */
    workplaceAvailability: {
      workplaceName: string;
      remainingCapacity: number;
      totalCapacity: number;
    };

    /**
     * The message used to display the time interval.
     *
     * `date` contains the full date-time of the interval.
     *
     * `duration` contains the duration of the interval compared to the start time in minutes.
     */
    intervalTime: { date: Date; duration: number };

    /**
     * This error message is shown when the selected date or time is invalid.
     */
    invalidDate: never;

    /**
     * The error message shown when the capacity of
     * a given time frame at a workplace has been reached.
     */
    maxCapacityError: never;
  }

  interface Actions {
    /**
     * This action is used to determine the count of reservations on a day, time, and location.
     */
    count: never;

    /**
     * This is fired once when the location data is requested.
     */
    getWorkplaces: never;

    /**
     * This action is used to fetch the user’s own reservations.
     */
    getOwnReservations: never;

    /**
     * This action is used to get all the reservations, used to calculate the remaining capacity.
     */
    getAllReservations: never;

    /**
     * This action is used to fetch the user’s previous preferences when creating a new reservation.
     */
    getPreferences: never;

    /**
     * This action is used to update the user’s preferences
     * if previous settings were found.
     */
    updatePreferences: never;

    /**
     * This action is used to create the user’s preferences
     * if previous settings were found.
     */
    createPreferences: never;
  }

  interface EventListeners {
    /**
     * This event will trigger the capacity validation.
     */
    changed: never;

    /**
     * This event will trigger the time option calculation
     * as well as checking for any overlapping reservations.
     */
    dateChanged: never;

    /**
     * This event will validate all fields for validity.
     */
    validateAll: never;

    /**
     * This event will filter and submit the list of available workplaces.
     */
    typesSelected: never;
  }

  interface EventEmitters {
    /**
     * This event is sent as an empty object
     * or an error depending on whether or not the reservation is valid.
     */
    validatedReservation: never;

    /**
     * This event is sent with the input data
     * or an error depending on whether or not all fields are valid.
     */
    validatedAll: never;

    /**
     * The available time options given the currently selected date and time.
     */
    time: never;

    /**
     * This event is sent after the user has selected a start date/time and duration as an
     * empty object or an error depending on whether or not there is an overlapping reservation.
     */
    validatedDate: never;

    /**
     * An event containing enum options with the list of workplaces
     * based on the selected type of workplace.
     *
     * Emitted after selecting a different workplace type.
     */
    workplaces: never;

    /**
     * An event that emits the last selected desk type and workplace type.
     */
    preferences: never;
  }

  interface Parameters {
    /**
     * The maximum time that users can make reservations.
     *
     * @pattern ^([01]\d|2[0-3]):([0-5]\d)$
     * @example '15:20'
     */
    maxTime: string;

    /**
     * The minimum time that users can make reservations.
     *
     * @pattern ^([01]\d|2[0-3]):([0-5]\d)$
     * @example '15:20'
     */
    minTime: string;
  }
}
