A block that fetches data, reformats the data and emits it using the events API.

This block is used by the teambarometer app to format the surveys correctly and then load them into
a chart block.
