import { bootstrap } from '@appsemble/sdk';

import styles from './index.module.css';

const meetingHashRegex = /\w{65}/;

bootstrap(({ data, events, parameters: { meetingId }, shadowRoot, utils }) => {
  const errorNode = document.createElement('article');
  errorNode.className = `my-4 message is-danger ${styles.error}`;
  const errorMessage = document.createElement('div');
  errorMessage.className = 'message-body';
  errorMessage.textContent = utils.formatMessage('loadMeetingError');
  errorNode.append(errorMessage);

  function setupMeetingIFrame(id: string): void {
    if (!meetingHashRegex.test(id)) {
      shadowRoot.append(errorNode);
      return;
    }

    const iframe: HTMLIFrameElement = document.createElement('iframe');
    iframe.style.width = '100%';
    iframe.style.height = '100%';
    iframe.style.boxSizing = 'border-box';

    iframe.src = `https://sandbox.weseedo.nl/meeting/${id}?`;
    iframe.setAttribute('allow', 'camera;microphone');

    shadowRoot.append(iframe);
  }

  const hasEvent = events.on.data((d) => {
    const id = utils.remap(meetingId, d) as string;
    setupMeetingIFrame(id);
  });

  if (!hasEvent) {
    const id = utils.remap(meetingId, data) as string;
    setupMeetingIFrame(id);
  }
});
