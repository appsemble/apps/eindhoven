declare module '@appsemble/sdk' {
  interface EventListeners {
    /**
     * The event to listen on for new meeting ID.
     */
    data: never;
  }

  interface Parameters {
    /**
     * The ID of the meeting.
     */
    meetingId: Remapper;
  }

  interface Messages {
    /*
     * The error to show when the meetingId is invalid.
     */
    loadMeetingError: never;
  }
}
