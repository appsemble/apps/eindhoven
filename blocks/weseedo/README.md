This block allows you to attend a WeSeeDo meeting. Pass the ID of the meeting to the block's
`meetingId` parameter or by using the `data` event.

WeSeeDo is an external service that allows you to create and attend online video-chats. For more
info, see https://www.weseedo.nl/.
