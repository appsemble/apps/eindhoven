The Reservation App is designed to provide employees with an available workplace in a simple way.
Whether you need an individual workplace or want to reserve a meeting room for collaboration, this
app makes the process seamless and efficient.

## Most important features

### Availability on multiple devices

Use the app on your private mobile, tablet or PC.

### Easy login

Log in with your own municipal login account (SSO). The app remembers your details, so you don't
have to log in again.

### Overview of reservations

Immediately see an overview of future reservations you have made.

### Easy reservations

Add a new reservation by checking whether there is still room available on a date and then adding
it.

### Flexible booking options

Book for the morning, afternoon or the whole day.

### Optional reservation details

Enter a reason for your reservation, choose from standard reasons, and optionally make a comment
with your reservation.

### Easy management of reservations

Easily adjust or delete a reservation.

### Calendar integration

Add a reservation you made to your calendar (iCal).

For managers, the app offers additional features, such as viewing reservations by date and by
employee, to help with management.

The Reservation App is designed with simplicity and clarity in mind. You'll know at a glance where
you stand. Try it out today and experience the ease of making your workplace reservation with the
Reservation App!
