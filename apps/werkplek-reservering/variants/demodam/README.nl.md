De Reserveer App is ontworpen om medewerkers op een eenvoudige manier een beschikbare werkplek te
laten reserveren. Of je nu een individuele werkplek nodig hebt of een vergaderruimte wilt reserveren
voor samenwerking, deze app maakt het proces naadloos en efficiënt.

## Belangrijkste kenmerken

### Beschikbaarheid op meerdere apparaten

Gebruik de app op je privé mobiel, tablet of PC.

### Eenvoudig inloggen

Log in met je eigen gemeente login account (SSO). De app onthoudt je gegevens, zodat je niet elke
keer opnieuw hoeft in te loggen.

### Overzicht van reserveringen

Bekijk meteen een overzicht van toekomstige reserveringen die je hebt gemaakt.

### Eenvoudig reserveren

Voeg een nieuwe reservering toe door op een datum te zien of er nog plek beschikbaar is en deze dan
vast te leggen.

### Flexibele reserveringsopties

Reserveer voor de ochtend, de middag of de hele dag.

### Optionele reserveringsdetails

Geef een reden op voor je reservering, kies uit standaard redenen, en maak optioneel een opmerking
bij je reservering.

### Eenvoudig beheer van reserveringen

Pas eenvoudig een reservering aan of verwijder deze.

### Agenda-integratie

Voeg een gemaakte reservering toe aan je agenda (iCal).

Voor managers biedt de app extra functies, zoals het bekijken van reserveringen per datum en per
medewerker, om te helpen bij het sturen op de dagspiegel.

De Reserveer App is ontworpen met het oog op eenvoud en overzichtelijkheid. In één oogopslag weet je
waar je aan toe bent. Probeer het vandaag nog uit en ervaar het gemak van het reserveren van je
werkplek met de Reserveer App!
