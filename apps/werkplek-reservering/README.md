"Werkplek reserveringen" is a tool for employees of municipality Eindhoven to reserve their
workplace in one of the city offices.

## Domain

The production version is hosted at https://werkplekken.eindhoven.nl

## SSL certificates

Appsemble is responsible for creating and sending a new CSR to Eindhoven each year, so they can
request a new SSL certificate from their vendor.

The private key and CSR can be created using the following script:

```
openssl req -new -newkey rsa:4096 -nodes -keyout werkplekken.eindhoven.nl.key -out werkplekken.eindhoven.nl.csr -subj "/C=NL/ST=Noord-Brabant/L=Eindhoven/O=Gemeente Eindhoven/OU=IT/CN=werkplekken.eindhoven.nl"
```

Put `werkplekken.eindhoven.nl.key` in Bitwarden, send the `werkplekken.eindhoven.nl.csr` to
Eindhoven.

It's best practice to also create a new private key for each new CSR.
