The “Team Barometer” is an app used to monitor the engagement and well-being of team members. There
are two sets of questions that are used.

The first set contains questions about the individual's involvement in changes in their lives,
working environment, their sense of information provision about sector developments, pride in their
work, teamwork and their current vitality (physical and mental fitness). There is also a question in
which team members are encouraged to identify improvements they want to work on.

The second set contains questions that focus on the team member's understanding of how their work
contributes to the objectives of the organization, their involvement in the change program within
the department, their perception of decision-making, team dynamics, collaboration, personal
enthusiasm and vitality.

The app is best used in an environment where continuous improvement is encouraged and where feedback
is appreciated. The goal is to promote a positive work environment where team members feel involved,
informed and vital.
