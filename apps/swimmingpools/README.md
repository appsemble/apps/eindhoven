The Appsemble Inspection App is a versatile tool designed to simplify the inspection process in
facilities in various ways. Although our example focuses on swimming pool inspections, the
application can be configured to inspect any facility, making it a truly customizable solution.

## Most important features

### Location selection

Users can choose from multiple locations, such as different swimming pools within a municipality.

### Area selection

Within each location, users can select specific areas (e.g. the 50m pool, the 25m swimming pool,
etc.) from a predefined list.

### Version control

The app is designed to accommodate variations in questions between pools, making it possible to
maintain multiple versions of the app.

### Reporting and approval

The generated report must be approved by someone else. The first assessor verifies the report, signs
it and automatically sends an email to the second person who verifies and signs the report.

### User management

There is a way to manage users (team leaders), including users' email, name, etc.

## Flow

### Compile daily report

Open app > select the correct location/swimming pool > fill in information during the day > last
person sends email

### Verify daily report

Look at email > link to the report in the app > possibly edit the report as necessary > signs it >
app sends a new email to the second reviewer > same steps…

### Configure

Configure team > edit person… etc.

### Locations

Configure locations > location > pools…

This app is intended to facilitate the daily work of swimming pool employees, who have to fill out
forms several times a day for safety and to maintain qualification for the quality mark. It is a
versatile tool that can be adapted to the specific needs of your facility. With the Appsemble
Inspection App you can streamline your inspection process and make it more efficient.
