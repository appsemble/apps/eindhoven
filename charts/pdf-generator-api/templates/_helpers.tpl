{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "pdf-generator-api.fullname" -}}
  {{- printf "%s-%s" .Release.Name .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "pdf-generator-api.chart" -}}
  {{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
The Docker image including repository and tag.
*/}}
{{- define "pdf-generator-api.image" -}}
  {{- printf "%s:%s" .Values.image.repository .Values.image.tag | quote -}}
{{- end -}}

{{/*
The URL to the instance, including the host name and protocol.
*/}}
{{- define "pdf-generator-api.url" -}}
  {{- printf "http://%s.%s.svc.cluster.local" (include "pdf-generator-api.fullname" .) .Release.Namespace -}}
{{- end -}}
