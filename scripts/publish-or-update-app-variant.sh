#!/bin/sh

# Input parameters: APP, VARIANT (optional, defaults to "development"), CONTEXT (optional)
APP=${1}
VARIANT=${2:-development}  # Default VARIANT to "development" if not provided
CONTEXT=${3}

# Define paths
app_path="apps/$APP"
app_rc_path="$app_path/.appsemblerc.yaml"  # Path to app's .appsemblerc.yaml file

# Construct context identifier
context=$VARIANT${CONTEXT:+-$CONTEXT}  # Combine VARIANT and CONTEXT with a hyphen if CONTEXT is provided

# Attempt to retrieve context_id from .appsemblerc.yaml, suppress errors
context_id=$(yq -e ".context.$context.id" "$app_rc_path" 2>/dev/null || true)

# Construct app name
app_name="$APP-$context"

# Attempt to update the app with the specified context
if npx appsemble -vv app update --context "$context" --force "$app_path"; then
  # If update is successful, print success message
  echo "Successful update on app $app_name";
else
  # If update fails, check if a context_id was retrieved
  if [ -n "$context_id" ] && [ "$context_id" != "null" ]; then
     # If context_id exists and is not null, the app with the context_id does not exist, publish instead of updating
    echo "App with $context id $context_id does not exist, publishing instead of updating";
  else
    # If context_id does not exist or is null, the app has no context id, publish instead of updating
    echo "App $APP has no $context id, publishing instead of updating";
  fi

  # Save a copy of the .appsemblerc.yaml file before publishing
  file_before="$(mktemp)"
  cp "$app_rc_path" "$file_before"

  # Publish the app with the specified context
  npx appsemble -vv app publish --context "$context" --modify-context "$app_path"

  # Export the published variable
  export PUBLISHED="true"

  # Apply changes to the .appsemblerc.yaml file to the staging area
  git diff --no-index --patch "$file_before" "$app_rc_path" |
    sed "s/--- a.*$/--- a\/apps\/$APP\/.appsemblerc.yaml/" |
    git apply --cached

  # Clean up temporary file
  rm "$file_before"
fi
